﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeCore
{
    // list to hold all the side cube info
    private List<List<GameObject>> allFaces = null;
    private List<List<GameObject>> allRays = null;

    private List<GameObject> rays = null;

    public CubeCore(List<GameObject> _rays)
    {
        rays = _rays;

        Init();
        BuildCubeInfo();
    }

    ~CubeCore()
    {

    }

    void Init()
    {
        allRays = new List<List<GameObject>>();
        allFaces = new List<List<GameObject>>();
    }

    void BuildCubeInfo()
    {
        BuildRays();
        BuildFaces();
    }

    void BuildRays()
    {
        for (int i = 0; i < (int)EFace.TOTAL; i++)
        {
            List<GameObject> temp = new List<GameObject>();

            int child = rays[i].transform.childCount;
            for (int j = 0; j < child; j++)
            {
                temp.Add(rays[i].transform.GetChild(j).gameObject);
            }
            allRays.Add(temp);
        }
    } 

    public void BuildFaces()
    {
        allFaces.Clear();
        RaycastHit hit;
        for (int i = 0; i < (int)EFace.TOTAL; i++)
        {
            List<GameObject> facesHit = new List<GameObject>();

            for (int j = 0; j < allRays[i].Count; j++)
            {
                Transform t = allRays[i][j].transform;
                if (Physics.Raycast(t.position, t.forward, out hit, 1f, Constants.layerMask))
                {
                    //Debug.DrawRay(t.position, t.forward * hit.distance, Color.green);
                    hit.transform.GetComponent<Face>().SetFace((EFace)i);
                    facesHit.Add(hit.transform.gameObject);
                    //Debug.Log("Ray: " + t.parent.name+" Face: "+hit.transform.parent.name);
                }
                else
                {
                    facesHit.Add(null);
                    //Debug.DrawRay(t.position, t.forward * 5, Color.red);
                }
            }
            allFaces.Add(facesHit);
        }
    }

    public List<List<GameObject>> AllFaces
    {
        get { return allFaces; }
    }

    public Vector3 GetRotationAngle(int _face, ESwipe _swipe)
    {
        Vector3 _dir = Vector3.zero;
        switch(_face)
        {
            case (int)EFace.FRONT:
                _dir = _swipe == ESwipe.UP || _swipe == ESwipe.LEFT ? Vector3.forward : Vector3.back;
                break;
            case (int)EFace.BACK:
                _dir = _swipe == ESwipe.UP || _swipe == ESwipe.LEFT ? Vector3.back : Vector3.forward;
                break;
            case (int)EFace.LEFT:
                _dir = _swipe == ESwipe.UP || _swipe == ESwipe.LEFT ? Vector3.right : Vector3.left;
                break;
            case (int)EFace.RIGHT:
                _dir = _swipe == ESwipe.UP || _swipe == ESwipe.LEFT ? Vector3.left : Vector3.right;
                break;
            case (int)EFace.UP:
                _dir = _swipe == ESwipe.UP || _swipe == ESwipe.LEFT ? Vector3.down : Vector3.up;
                break;
            case (int)EFace.DOWN:
                _dir = _swipe == ESwipe.UP || _swipe == ESwipe.LEFT ? Vector3.up : Vector3.down;
                break;
            default:
                break;
        }
        return (_dir*90);
    }

    public bool IsSolved()
    {
        bool solved = false;
        for (int i = 0; i < allFaces.Count; i++)
        {
            EColor col = allFaces[i][0].GetComponent<Face>().color; //first element in list
            solved = allFaces[i].TrueForAll(x=>x.GetComponent<Face>().color.Equals(col));
            if (!solved) break;
        }

        return solved;
    }

    /*
        EColor col = core.AllFaces[0][0].GetComponent<Face>().color;
        bool val = core.AllFaces[0].TrueForAll(x=>x.GetComponent<Face>().color.Equals(EColor.RED));
        Debug.Log("All Front FAce1: " + col+" - "+val);
     */

}
