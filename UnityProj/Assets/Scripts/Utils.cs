﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utils
{

    public static string GetFormattedTime(int ticks)
    {
        string mFormatedString = string.Empty;
        //int remaining_time = (int)aTimeSpan.TotalSeconds;

        int Days = Mathf.FloorToInt(ticks / 86400);
        int Hours = Mathf.FloorToInt((ticks - Days * 86400) / 3600) % 3600;
        int minutes = Mathf.FloorToInt((ticks) / 60) % 60;
        int seconds = Mathf.FloorToInt(ticks - minutes * 60) % 60;

        if (Days > 0)
        {
            mFormatedString = string.Format("{0:00}:{1:00}:{2:00}:{3:00}", Days, Hours, minutes, seconds);
        }
        else if (Hours > 0)
        {
            mFormatedString = string.Format("{0:00}:{1:00}:{2:00}", Hours, minutes, seconds);
        }
        else //if (minutes > 0)
        {
            mFormatedString = string.Format("{0:00}:{1:00}", minutes, seconds);
        }
        //else
        //{
        //    mFormatedString = string.Format("{0:00s}", seconds);
        //}

        return mFormatedString;
    }
}
