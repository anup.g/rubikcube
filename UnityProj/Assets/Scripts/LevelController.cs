﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour
{
    void Start()
    {
        
    }

    public void CubeSizeClicked(string val)
    {
        Debug.Log("CubeSize: " + val);
        AppManager.Instance.selectedCubeSize = val;
        Fader.Instance.ChangeScene((int)EScene.GAME);
    }

    public void BackClicked()
    {
        Fader.Instance.ChangeScene((int)EScene.MENU);
    }

}
