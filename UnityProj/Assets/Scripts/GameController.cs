﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class GameController : MonoBehaviour
{
    private static GameController _instance = null;
    public Text timer;
    private bool isGameStart = false, isGameOver = false;
    public Action gameStart, undoTurn, gameOver;
    [HideInInspector] public int timeCounter = 0;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);
    }

    void Start()
    {
        timer.text = string.Empty;
        CreateCube();
    }

    private void OnEnable()
    {
        gameStart += GameStart;
        gameOver += GameOver;
    }

    private void OnDisable()
    {
        gameStart -= GameStart;
        gameOver -= GameOver;
    }

    public static GameController Instance
    {
        get { return _instance; }
        private set { _instance = value; }
    }

    void CreateCube()
    {
        string cubeToLoad = "cubes/Cube" + AppManager.Instance.selectedCubeSize;
        GameObject cube = Resources.Load<GameObject>(cubeToLoad);
        Instantiate(cube);
    }

    public bool IsGamePlaying
    {
        get { return isGameStart; }
        set { isGameStart = value; }
    }

    public bool IsGameOver
    {
        get { return isGameOver; }
        private set { isGameOver = value; }
    }

    void GameStart()
    {
        Debug.Log("** Game Start **");
        IsGamePlaying = true;
        UIManager.Instance.ShowScreen((int)EGameScreen.GAME);
        StartCoroutine(StartTimer());
    }

    void GameOver()
    {
        Debug.Log("** Game Over **");
        IsGamePlaying = false;
        IsGameOver = true;
        StopAllCoroutines();
        Invoke("ShowGameOver", 10f);
    }

    IEnumerator StartTimer()
    {
        while(true)
        {
            yield return new WaitForSeconds(1f);
            timeCounter += 1;
            timer.text = Utils.GetFormattedTime(timeCounter);
        }
    }

    void ShowGameOver()
    {
        UIManager.Instance.ShowScreen((int)EGameScreen.GAMEOVER);
    }

    public void MenuClicked()
    {
        if (IsGamePlaying)
            UIManager.Instance.ShowScreen((int)EGameScreen.MENU);
    }

    public void UndoClicked()
    {
        if(IsGamePlaying)
            undoTurn.Invoke();
    }
}
