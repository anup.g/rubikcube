﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System.Linq;

public class CubeController : MonoBehaviour
{
    [SerializeField] private Transform pivot = null;
    private CubeCore core = null;

    [SerializeField] private List<GameObject> rays= null;
    private Transform camTransform = null;
    private List<Scramble> scrambleData = null, reverseScrambleData = null;

    Vector2 firstPressPos, secondPressPos, swipeDirection;
    RaycastHit hit;
    int selectedFace= -1;

    void Start()
    {
        Init();
        GenerateScrambleInfo();
        Invoke("StartScramble", 2f);
    }

    void Init()
    {
        scrambleData = new List<Scramble>();
        reverseScrambleData = new List<Scramble>();

        if (core == null)
            core = new CubeCore(rays);

        camTransform = Camera.main.transform;
    }

    private void OnEnable()
    {
        GameController.Instance.undoTurn += UndoTurn;
    }

    private void OnDisable()
    {
        GameController.Instance.undoTurn -= UndoTurn;
    }

    void Update() 
    {
        if (GameController.Instance.IsGamePlaying)
        {
            //while the cube rotate animation is happening dont accept input
            if (!DOTween.IsTweening(pivot))
            {
                // Rotate cube to look at All sides
                if (Input.GetMouseButton(1))
                    CubeLookRotate();

                CubeSwipe();
            }
        }
    } 

    void GenerateScrambleInfo()
    {
        for (int i = 0; i < Constants.scrambleLimit; i++)
        {
            Scramble sc = new Scramble();
            sc.face = Random.Range(0, (int)EFace.TOTAL);
            sc.swipe = (ESwipe)Random.Range(0, (int)ESwipe.TOTAL);
            scrambleData.Add(sc);
        }
    }

    void StartScramble()
    {
        StartCoroutine(ProcessScramble());
    }

    IEnumerator ProcessScramble()
    {
        for (int i = 0; i < scrambleData.Count; i++)
        {
            selectedFace = scrambleData[i].face;
            RotateCube(selectedFace, scrambleData[i].swipe);
            yield return new WaitForSeconds(0.4f);
        }

        scrambleData.Clear();
        Debug.Log("Scramble Done");
        GameController.Instance.gameStart.Invoke();
    }

    void CubeLookRotate()
    {
        float rotationX = Input.GetAxis("Mouse X") * 5;
        float rotationY = Input.GetAxis("Mouse Y") * 5;
        transform.Rotate(camTransform.up, -Mathf.Deg2Rad * rotationX * Time.deltaTime * 5000, Space.World);
        transform.Rotate(camTransform.right, Mathf.Deg2Rad * rotationY * Time.deltaTime * 5000, Space.World);
    }

    public void CubeSwipe()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 25f, Constants.layerMask))
            {
                //Debug.Log("SF: " + hit.transform.name+" "+hit.transform.parent.name);
                selectedFace = (int)hit.transform.GetComponent<Face>().faceType;
            }
                
            firstPressPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        }
        if (Input.GetMouseButtonUp(0))
        {
            if (secondPressPos.Equals(firstPressPos) || selectedFace == -1) return;

            secondPressPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            swipeDirection = (secondPressPos - firstPressPos).normalized;

            //Swipe up
            if (swipeDirection.y > 0.0f && swipeDirection.x > -0.7f && swipeDirection.x < 0.7f)
            {
                RotateCube(selectedFace, ESwipe.UP);
                reverseScrambleData.Add(new Scramble(selectedFace, ESwipe.DOWN));
            }
            //swipe down
            else if (swipeDirection.y < 0.0f && swipeDirection.x > -0.7f && swipeDirection.x < 0.7f)
            {
                RotateCube(selectedFace, ESwipe.DOWN);
                reverseScrambleData.Add(new Scramble(selectedFace, ESwipe.UP));
            }
            //swipe left
            else if (swipeDirection.x < 0.0f && swipeDirection.y > -0.7f && swipeDirection.y < 0.7f)
            {
                RotateCube(selectedFace, ESwipe.LEFT);
                reverseScrambleData.Add(new Scramble(selectedFace, ESwipe.RIGHT));
            }
            //swipe right
            else if (swipeDirection.x > 0.0f && swipeDirection.y > -0.7f && swipeDirection.y < 0.7f)
            {
                RotateCube(selectedFace, ESwipe.RIGHT);
                reverseScrambleData.Add(new Scramble(selectedFace, ESwipe.LEFT));
            }
        }
    }

    public void RotateCube(int _face, ESwipe _swipe)
    {
        // parent faces cube to pivot
        for (int i = 0; i < core.AllFaces[_face].Count; i++)
            core.AllFaces[_face][i].transform.parent.parent = pivot;

        Vector3 _dir = core.GetRotationAngle(_face, _swipe);
        pivot.DOLocalRotate(_dir, 0.2f, RotateMode.LocalAxisAdd).OnComplete(()=>
        {
            Debug.Log("rotate complete");
            selectedFace = -1;
            // unparent faces cube from pivot
            for (int i = 0; i < core.AllFaces[_face].Count; i++)
                core.AllFaces[_face][i].transform.parent.parent = transform;

            pivot.localRotation = Quaternion.Euler(Vector3.zero);
            core.BuildFaces();

            if(GameController.Instance.IsGamePlaying)
                CheckCubeSolved();
        });
    }

    void UndoTurn()
    {
        if (reverseScrambleData.Count == 0) return;
        Scramble sc = reverseScrambleData.Last<Scramble>();
        RotateCube(sc.face, sc.swipe);
        reverseScrambleData.Remove(sc);
    }

    void CheckCubeSolved()
    {
        bool solved = core.IsSolved();
        if (solved)
            GameController.Instance.gameOver.Invoke();
    }
}
