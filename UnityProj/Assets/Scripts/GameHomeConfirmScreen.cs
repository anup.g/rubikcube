﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameHomeConfirmScreen : MonoBehaviour
{
    void Start()
    {
        
    }

    public void YesClicked()
    {
        Fader.Instance.ChangeScene((int)EScene.MENU);
    }

    public void NoClicked()
    {
        UIManager.Instance.ShowScreen((int)EGameScreen.MENU);
    }
}
