﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppManager : MonoBehaviour
{
    private static AppManager _instance = null;

    [HideInInspector] public string selectedCubeSize = string.Empty;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {

    }

    public static AppManager Instance
    {
        get { return _instance; }
        private set { _instance = value; }
    }

}
 