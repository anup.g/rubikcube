﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "file", menuName = "ScriptableObjects/CamSettingsScriptableObject", order = 0)]
public class CamSettingsScriptableObject : ScriptableObject
{
    public CamConfig settings;
}
