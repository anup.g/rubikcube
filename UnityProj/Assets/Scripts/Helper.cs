using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum EScene { SPLASH, MENU, LEVEL, GAME };
public enum EConfirm { NO, YES };
public enum EFace { FRONT, BACK, UP, DOWN, LEFT, RIGHT, TOTAL };
public enum EColor { RED, ORANGE, WHITE, YELLOW, GREEN, BLUE };
public enum ESwipe { LEFT, RIGHT, UP, DOWN, TOTAL };
public enum EUI { TOGGLE, OVERLAP };
public enum EGameScreen { GAME, MENU, RESTARTCONFIRM, HOMECONFIRM, GAMEOVER };

public class Constants
{
    public const int layerMask = 1 << 8; //face layer
    public const int scrambleLimit = 20;
}

[Serializable]
public class Scramble
{
    public int face;
    public ESwipe swipe;

    public Scramble()
    {
        face = 0;
        swipe = ESwipe.LEFT;
    }

    public Scramble(int _face, ESwipe _swipe)
    {
        face = _face;
        swipe = _swipe;
    }
}

[Serializable]
public class UIHolder
{
    public GameObject screen;
    public EUI type;
}

[Serializable]
public class CamConfig
{
    public Vector3 camPosition;
    public float minFov;
    public float maxFov;
}