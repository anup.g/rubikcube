﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameMenuScreen : MonoBehaviour
{
    [SerializeField] private Text timerToggle = null;
    GameObject timer;

    void Start()
    {
        timer = GameController.Instance.timer.gameObject;
        if(timer.activeSelf)
            timerToggle.text = "Timer OFF";
        else
            timerToggle.text = "Timer ON";
    }

    public void TimerToggle()
    {
        if (timer.activeSelf)
        {
            timer.SetActive(false);
            timerToggle.text = "Timer ON";
        }
        else
        {
            timer.SetActive(true);
            timerToggle.text = "Timer OFF";
        }
    }

    public void Restart()
    {
        UIManager.Instance.ShowScreen((int)EGameScreen.RESTARTCONFIRM);
    }

    public void GoToHome()
    {
        UIManager.Instance.ShowScreen((int)EGameScreen.HOMECONFIRM);
    }

    public void CloseDialog()
    {
        UIManager.Instance.ShowScreen((int)EGameScreen.GAME);
    }
}
