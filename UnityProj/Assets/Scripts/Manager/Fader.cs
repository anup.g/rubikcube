using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Fader : MonoBehaviour
{
    private static Fader _instance = null;

    private string[] animState = { "idle", "in", "out" };
    private enum EState { IDLE, IN, OUT };

    [SerializeField] private Animator animatorRef = null;
    
    int sceneToLoad = 0;
    bool sceneLoaded = false;

    void Awake() 
    {
        if(Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void Start()
    {
        //animatorRef.speed = 0.2f;
        ChangeState = EState.IDLE;
    }

    public static Fader Instance
    {
        get { return _instance; }
        private set { _instance = value; }
    }

    public void ChangeScene(int _scene)
    {
        sceneToLoad = _scene;
        StartCoroutine(LoadScene());
    }

    IEnumerator LoadScene()
    {
        sceneLoaded = false;

        ChangeState = EState.IN;
        yield return new WaitUntil(() => sceneLoaded == true);
        ChangeState = EState.OUT;
    }


    private EState ChangeState
    {
        set { animatorRef.SetTrigger(animState[(int)value]); }
    }

    private void OnAnimFadeInDone()
    {
        SceneManager.LoadScene(sceneToLoad);
    }

    private void OnAnimFadeOutDone()
    {
        ChangeState = EState.IDLE;
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        sceneLoaded = true;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }
}
