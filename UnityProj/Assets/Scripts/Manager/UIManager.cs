﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    private static UIManager _instance = null;
    private GameObject newScreen = null, currScreen = null, prevScreen = null;

    public UIHolder[] holder;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);
    }

    void Start()
    {
        StartWithDefault();
    }

    public static UIManager Instance
    {
        get { return _instance; }
        private set { _instance = value; }
    }

    void StartWithDefault()
    {
        for (int i = 0; i < holder.Length; i++)
        {
            holder[i].screen.SetActive(false);
        }
    }

    public void ShowScreen(int _idx)
    {
        Debug.Assert(holder[_idx].screen != null, "Screen is NULL");
        newScreen = holder[_idx].screen;
        if (currScreen != null && holder[_idx].type == EUI.TOGGLE)
            currScreen.SetActive(false);

        prevScreen = currScreen == null ? newScreen : currScreen;
        currScreen = newScreen;

        Debug.Log("Curr Screen: " + currScreen.name + " Prev Screen: " + prevScreen.name);
        currScreen.SetActive(true);
    }

    public void PrevScreen()
    {
        Debug.Assert(prevScreen != null, "Screen is NULL");
        newScreen = prevScreen;
        currScreen.SetActive(false);

        prevScreen = currScreen;
        currScreen = newScreen;

        currScreen.SetActive(true);
    }

    public string CurrentScreen
    {
        get { return currScreen.name; }
    }
    public string PreScreen
    {
        get { return prevScreen.name; }
    }
}
