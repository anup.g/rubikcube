﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverScreen : MonoBehaviour
{
    [SerializeField] Text timeTaken;

    void Start()
    {
        
    }

    private void OnEnable()
    {
        timeTaken.text = "TimeTaken: " + Utils.GetFormattedTime(GameController.Instance.timeCounter);
    }

    public void PlayAgainClicked()
    {
        Fader.Instance.ChangeScene((int)EScene.GAME);
    }

    public void HomeClicked()
    {
        Fader.Instance.ChangeScene((int)EScene.MENU);
    }

    public void CloseClicked()
    {
        UIManager.Instance.PrevScreen();
    }
    
}
