﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Face : MonoBehaviour
{
    public EFace faceType;
    public EColor color;

    public void SetFace(EFace _face)
    {
        faceType = _face;
    }
}
