﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplashController : MonoBehaviour
{
    void Start()
    {
        Invoke("GoToMenu", 3f);
    }

    void GoToMenu()
    {
        Fader.Instance.ChangeScene((int)EScene.MENU);
    }

}
