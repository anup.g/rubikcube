﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameRestartConfirmScreen : MonoBehaviour
{
    void Start()
    {
        
    }

    public void YesClicked()
    {
        Fader.Instance.ChangeScene((int)EScene.GAME);
    }

    public void NoClicked()
    {
        UIManager.Instance.ShowScreen((int)EGameScreen.MENU);
    }
}
