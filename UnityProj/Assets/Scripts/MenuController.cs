﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class MenuController : MonoBehaviour
{
    [SerializeField] GameObject quitScreen = null;

    void Start()
    {
        
    }

    public void NewGameClicked()
    {
        Fader.Instance.ChangeScene((int)EScene.LEVEL);
    }

    //Implementation not complete
    //public void SavedGameClicked()
    //{
    //    //Fader.Instance.ChangeScene((int)EScene.Game);
    //    Debug.Log("Saved Game Clicked");
    //}

    public void QuitGameClicked()
    {
        quitScreen.SetActive(true);
    }

    public void QuitConfirm(int val)
    {
        if (val == (int)EConfirm.YES)
            QuitGame();
        else
            quitScreen.SetActive(false);
    }

    private void QuitGame()
    {
#if UNITY_EDITOR
        EditorApplication.ExitPlaymode();
#else
        Application.Quit();
#endif
    }
}
