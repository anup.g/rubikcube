﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamController : MonoBehaviour
{
    private Camera camRef = null;
    private CamSettingsScriptableObject camSo = null;

    // zoom in/out
    [SerializeField] float zoomSpeed = 0;
    float fovMin = 0;
    float fovMax = 0;

    // rotate around target
    Transform target = null;
    [SerializeField] float speed = 0f;

    void Start()
    {
        if(camRef == null)
            camRef = GetComponent<Camera>();

        if(camSo == null)
        {
            string path = "camConfig/cam" + AppManager.Instance.selectedCubeSize;
            camSo = Resources.Load<CamSettingsScriptableObject>(path);

            // apply values on camera
            transform.position = camSo.settings.camPosition;
            fovMin = camSo.settings.minFov;
            fovMax = camSo.settings.maxFov;
        }
    }

    private void OnEnable()
    {
        GameController.Instance.gameOver += GameOver;
    }

    private void OnDisable()
    {
        GameController.Instance.gameOver -= GameOver;
    }

    void GameOver()
    {
        target = GameObject.FindGameObjectWithTag("rubik").transform;
    }

    void Update()
    {
        // Zoom IN/OUT with mouse scroll Wheel
        if (GameController.Instance.IsGamePlaying)
        {
            if (Input.GetAxis("Mouse ScrollWheel") < 0)
                camRef.fieldOfView += zoomSpeed;
            if (Input.GetAxis("Mouse ScrollWheel") > 0)
                camRef.fieldOfView -= zoomSpeed;

            camRef.fieldOfView = Mathf.Clamp(camRef.fieldOfView, fovMin, fovMax);
        }

        // rotate camera after game over
        if (GameController.Instance.IsGameOver)
        {
            transform.LookAt(target);
            transform.RotateAround(target.position, Vector3.up, speed);
        }
    }
}
