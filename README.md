# RubikCube

Rubik's Cube Implementation

Unity Version Used: 2019.4.18f1

Note: Please start the project in the editor from Splash Scene

Project Controls:
- Mouse Right Button: used to rotate the cube
- Mouse Left Button: used to swipe on the screen for cubes face rotation
- Mouse Scroll: used to Zoom IN/OUT

Features Implemented:
- Game Flow from menu to gameplay
- Fader for screen fade IN/OUT
- Raycast to detect the faces of cube
- Added Undo move feature
- UIManager to manage the game screen UI
- Timer to count time taken by user
- 2x2x2 and 3x3x3 size cubes spawn at runtime depending on selection in menu
- Auto scramble 
- scriptable object to store the camera settings for 2x2x2 and 3x3x3 cube
- mouse rotate cube, swipe and zoom

Pending Tasks:
- Game Save needs to be implemented
- current logic can be expanded to 4x4x4, 5x5x5 and 6x6x6 cubes